package com.alifeincode.hcrdemoapp;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class AddingService extends IntentService {
    public static final String ACTION_ADD = "com.alifeincode.hcrdemoapp.action.ADD";
    public static final String ACTION_ADD_RESPONSE = "com.alifeincode.hcrdemoapp.action.ADD_RESPONSE";

    public static final String EXTRA_LPARAM = "com.alifeincode.hcrdemoapp.extra.LPARAM";
    public static final String EXTRA_RPARAM = "com.alifeincode.hcrdemoapp.extra.RPARAM";
    public static final String EXTRA_RESULT = "com.alifeincode.hcrdemoapp.extra.RESULT";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionAdd(Context context, int lparam, int rparam) {
        Intent intent = new Intent(context, AddingService.class);
        intent.setAction(ACTION_ADD);
        intent.putExtra(EXTRA_LPARAM, lparam);
        intent.putExtra(EXTRA_RPARAM, rparam);
        context.startService(intent);
    }

    public AddingService() {
        super("AddingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_ADD.equals(action)) {
                final int param1 = intent.getIntExtra(EXTRA_LPARAM, 0);
                final int param2 = intent.getIntExtra(EXTRA_RPARAM, 0);
                handleActionAdd(param1, param2);
            }
        }
    }

    /**
     * Handle action ADD in the provided background thread with the provided
     * parameters.
     */
    private void handleActionAdd(int param1, int param2) {
        int sum = param1 + param2;

        Intent response = new Intent();
        response.setAction(ACTION_ADD_RESPONSE);
        response.addCategory(Intent.CATEGORY_DEFAULT);
        response.putExtra(EXTRA_RESULT, sum);
        response.putExtra(EXTRA_LPARAM, param1);
        response.putExtra(EXTRA_RPARAM, param2);

        sendBroadcast(response);
    }
}
