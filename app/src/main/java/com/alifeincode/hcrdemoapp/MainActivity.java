package com.alifeincode.hcrdemoapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.ServerValue;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends ActionBarActivity {

    private SumReceiver results = new SumReceiver();
    private IntentFilter filter = new IntentFilter(AddingService.ACTION_ADD_RESPONSE);

    private EditText lval;
    private EditText rval;
    private Button btn;

    private Firebase fb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Firebase.setAndroidContext(this);
        fb = new Firebase("https://hcrdemoapp.firebaseIO.com");

        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(results, filter);

        lval = (EditText)findViewById(R.id.lval);
        rval = (EditText)findViewById(R.id.rval);

        final Context ctx = this;

        btn = (Button)findViewById(R.id.add_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, AddingService.class);
                intent.setAction(AddingService.ACTION_ADD);
                intent.putExtra(AddingService.EXTRA_LPARAM, lparam);
                intent.putExtra(AddingService.EXTRA_RPARAM, rparam);
                ctx.startService(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(results);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SumReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int sum = intent.getIntExtra(AddingService.EXTRA_RESULT, 0);
            Toast.makeText(context, "Sum: " + Integer.toString(sum), Toast.LENGTH_SHORT).show();

            Firebase ref = fb.child("logs").push();

            int left = intent.getIntExtra(AddingService.EXTRA_LPARAM, 0);
            int right = intent.getIntExtra(AddingService.EXTRA_RPARAM, 0);

            Map<String, Integer> log = new HashMap<String, Integer>();
            log.put("left", left);
            log.put("right", right);
            log.put("sum", sum);

            ref.setValue(log);
            ref.child("timestamp").setValue(ServerValue.TIMESTAMP);
        }
    }
}
